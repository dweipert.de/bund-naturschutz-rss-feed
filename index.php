<?php

use BundNaturschutz\RSS\Builder;
use BundNaturschutz\RSS\Router;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Serializer\Encoder\XmlEncoder;

require __DIR__ . '/vendor/autoload.php';

$response = new Response();

try {
  $router = new Router(Request::createFromGlobals());
  $request = $router->matchRequest();

  $group = $request->attributes->get('route')['group'];
  $groupUrl = 'https://' . $group . '.bund-naturschutz.de';

  $builder = new Builder($groupUrl . '/aktuelles');
  $rss = $builder->build();

  $response->setContent($rss);
  $response->headers->set('Content-Type', 'text/xml');
} catch (GuzzleException $exception) {
  $response->setStatusCode($exception->getCode());
  $response->setContent($exception->getMessage());
} catch (\Exception $exception) {
  $response->setStatusCode(Response::HTTP_BAD_REQUEST);
  $response->setContent($exception->getMessage());
}

$response->send();

