<?php

namespace BundNaturschutz\RSS;

use GuzzleHttp\Client;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Encoder\XmlEncoder;

class Builder
{
  public string $xml;

  public function __construct(
    public string $pageUrl,
  )
  {
    $client = new Client();
    $response = $client->get($pageUrl);

    $this->xml = (string)$response->getBody();
  }

  public function build(): string
  {
    $parsedUrl = parse_url($this->pageUrl);
    $url = "$parsedUrl[scheme]://$parsedUrl[host]";

    libxml_use_internal_errors(true);

    $doc = new \DOMDocument();
    $doc->loadHTML($this->xml);
    $xpath = new \DOMXPath($doc);

    $xml = [
      '@xmlns' => 'http://www.w3.org/2005/Atom',
      '@xmlns:media' => 'http://search.yahoo.com/mrss',
      'title' => 'BUND Naturschutz',
      'entry' => [],
    ];

    /**@var DOMNodeList $entries*/
    $entries = $xpath->evaluate('//div[@itemtype="http://schema.org/Article"]/a');

    foreach ($entries as $entry) {
      /**@var DOMElement $entry*/
      $href = $entry->attributes->getNamedItem('href')->textContent;
      $link = $url . $href;

      /**@var DOMNodeList $imgPath*/
      $imgPath = $xpath->evaluate('.//img', $entry);
      $img = '';
      if ($imgPath->length > 0) {
	$img = $imgPath->item(0)->attributes->getNamedItem('src')->textContent;
      }

      /**@var DOMNodeList $titlePath*/
      $titlePath = $xpath->evaluate('.//*[@itemprop="headline"]', $entry);
      $title = trim($titlePath->item(0)->textContent);

      /**@var DOMNodeList $datePath*/
      $datePath = $xpath->evaluate('.//*[@itemprop="datePublished"]', $entry);
      $date = $datePath->item(0)->attributes->getNamedItem('datetime')->textContent;

      /**@var DOMNodeList $summaryPath*/
      $summaryPath = $xpath->evaluate('.//*[@itemprop="description"]/text()', $entry);
      $summary = '';
      if ($summaryPath->length > 0) {
	$summary = '<p>' . trim($summaryPath->item(0)->textContent) . '</p>';
      }

      $readMoreLink = '<p><a href="' . $link . '">Lesen</a></p>';

      $xml['entry'][] = [
	'title' => $title,
	'link' => [
	  '@href' => $link,
	],
	'published' => $date,
	'content' => [
	  '@type' => 'html',
	  '#' => $summary . $readMoreLink,
	],
	'media:thumbnail' => [
	  '@url' => $url . $img,
	],
      ];
    }

    $encoder = new XmlEncoder([
      XmlEncoder::ROOT_NODE_NAME => 'feed',
    ]);

    $rss = $encoder->encode($xml, XmlEncoder::FORMAT, [
      'xml_encoding' => 'utf-8',
    ]);

    return $rss;
  }
}

