<?php

namespace BundNaturschutz\RSS;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

class Router
{
  public UrlMatcher $matcher;

  public function __construct(
    public Request $request
  )
  {
    $routes = new RouteCollection();
    $routes->add('group', new Route('/{group}', methods: ['GET']));

    $context = new RequestContext($request);
    $this->matcher = new UrlMatcher($routes, $context);
  }

  public function matchRequest(): Request
  {
    $match = $this->matcher->matchRequest($this->request);
    $this->request->attributes->set('route', $match);

    return $this->request;
  }
}

